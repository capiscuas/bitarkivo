#!/bin/sh

#http://esperanto-hamburg.de/wp-content/uploads/2015/03/EHH_96_01.pdf
url="http://esperanto-hamburg.de/wp-content/uploads/2015/03/EHH_"
for i in {1..10};
do 
  final_url="$url`printf "%02d" $i`.pdf";
  echo "$final_url"; 
done
