# HOW TO USE
# Ĉi tiu ilo ordigas.
# for i in *.pdf; do python reorderpagesPDF.py $i fixed/$i; done;

from PyPDF2 import PdfFileWriter, PdfFileReader
output = PdfFileWriter()

# wpdf = PdfFileReader(open('wmark.pdf', 'rb'))
# watermark = wpdf.getPage(0)
#
# for i in range(ipdf.getNumPages()):
#     page = ipdf.getPage(i)
#     #page.mergePage(watermark)
#     output.addPage(page)

import argparse
parser = argparse.ArgumentParser()
parser.add_argument("inputfile", help="unsorted PDF filename")
parser.add_argument("outputfile", help="final sorted PDF filename")
args = parser.parse_args()
ipdf = PdfFileReader(open(args.inputfile, 'rb'))
total_pagxoj = ipdf.getNumPages()
print('Ordigante', args.inputfile, 'al', args.outputfile, ". Paĝoj: ", total_pagxoj)
pagxordo = {4: [1, 2, 3, 0],
            6: [1, 2, 5, 3, 4, 0],
            8: [1, 2, 5, 6, 7, 4 , 3, 0]}
# print('Paĝ-ordo:', pagxordo)


if total_pagxoj in [4, 6, 8]:
    pagxoj = [ipdf.getPage(i) for i in range(total_pagxoj)]
    for i in pagxordo[total_pagxoj]:
        output.addPage(pagxoj[i])

    with open(args.outputfile, 'wb') as f:
        output.write(f)

else:
    print('Fuŝo:', args.inputfile, 'enhavas {0} paĝoj'.format(total_pagxoj))
