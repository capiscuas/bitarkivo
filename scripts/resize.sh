#!/bin/bash

FILE="$1"
DIMS=`identify "${FILE}" | awk '{print $3}'`
WIDTH=`echo "${DIMS}"| cut -dx -f1`
HEIGHT=`echo "${DIMS}"| cut -dx -f2`
max_w=1000
max_h=2000

if [[ $HEIGHT -gt $max_h ]]; then
    WIDTH=$(( $WIDTH * $max_h / $HEIGHT ))
    HEIGHT="$max_h"
fi


if [[ $WIDTH -gt $max_w ]]; then
    HEIGHT=$(( $HEIGHT * $max_w / $WIDTH ))
    WIDTH="$max_w"
fi


echo "$FILE ${WIDTH}x${HEIGHT}"
mogrify -filter Lanczos -resize "${WIDTH}x${HEIGHT}" "$FILE"
