#!/bin/bash

mkdir fixed
for f in *.pdf
do 
    i=${f%%.*}
    echo $i
    pdfcrop  $i;
    pdfimages $i-crop.pdf $i-crop
    for g in *.ppm
    do 
        convert $g $g.jpg
        bash ../../../../max1000width.sh $g.jpg 
    done;
    convert *.jpg fixed/$i.pdf
    rm *.ppm *.jpg
    rm $i-crop.pdf
done;
