#!/usr/bin/env python
# Por bildoj:
# for i in *.jpg; do convert -crop 50%x100% +repage $i $i; done;
# Ĉi tiu ilo disigas en du paĝoj kiam ĉiuj PDF-paĝoj kontenas 2 kune.
# for i in *.pdf; do python separate2pages.py <$i >fixed/$i; done;

import copy, sys
from pyPdf import PdfFileWriter, PdfFileReader
input = PdfFileReader(sys.stdin)
output = PdfFileWriter()
#cover no split
#p = input.getPage(0)
#output.addPage(p)
total_pages = input.getNumPages()
#other pages
#singlepages = [0,]
#doublepages = []
for p in [input.getPage(i) for i in range(0,total_pages)]:
    q = copy.copy(p)
    (w, h) = p.mediaBox.upperRight
    if w > 800:
        p.mediaBox.upperRight = (w/2, h)
        q.mediaBox.upperLeft = (w/2, h)
        output.addPage(p)
        output.addPage(q)
    else:
        output.addPage(p)
output.write(sys.stdout)
