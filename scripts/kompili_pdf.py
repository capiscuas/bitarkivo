# -*- coding: utf-8 -*-
#!/usr/bin/python

import os,sys
from wand.image import Image
from PyPDF2 import PdfFileWriter, PdfFileReader

import re


def krei_pdf(pdf_bildoj, pdf_nomo):
    blanka_pdf = '/home/ivan/workspace/django_bitarkivo/scripts/blank-a4.pdf'
    output = PdfFileWriter()
    print("Kreante PDF por %s" % pdf_nomo)
    for d in pdf_bildoj:
        print("Aldonante pagxo %s..." % d)
        try:
            with Image(filename=d) as img:
                img.format = 'jpeg'
                img.save(filename='temp.pdf')
            pdf = PdfFileReader(open('temp.pdf', "rb"))
            for page in pdf.pages:
                output.addPage(page)
            os.remove('temp.pdf')
        except:
            print('Fuŝita bildo:', d)
            pdf = PdfFileReader(open(blanka_pdf, "rb"))
            for page in pdf.pages:
                output.addPage(page)


    outputFile = "{0}.pdf".format(pdf_nomo)
    outStream = open(outputFile, "wb")
    output.write(outStream)
    outStream.close()


def chapsort(first, second):
    b1 = os.path.basename(first)
    b2 = os.path.basename(second)
    b1 = os.path.splitext(b1)[0]
    b2 = os.path.splitext(b2)[0]
    reg = "_page(\d+)"
    m1 = re.search(reg, b1, re.I)
    m2 = re.search(reg, b2, re.I)
    group_1 = int(m1.group(1))
    group_2 = int(m2.group(1))
    return cmp(group_1, group_2)

cwd = os.getcwd()
dosierujo = os.path.basename(cwd)

files = [file for file in os.listdir(cwd)]

image_extension = '.jpg'
indeksoj = {}
for f in files:
    reg = "(.+)_page(.+){0}".format(image_extension)
    m1 = re.search(reg, f, re.I)
    if m1:
        indekso = m1.group(1)
        if indekso not in indeksoj:
            indeksoj[indekso] = [f]
        else:
            indeksoj[indekso].append(f)

# print(indeksoj)
jaro = dosierujo
albumo = 'Esperanto-UEA'

dividi_jaro = {
               '008': {'num': '491', 'dato': "jan-mar"},
               '016': {'num': '492', 'dato': "apr-jun"},
            #    '028': {'num': '483', 'dato': "maj-jun"},
            #    '036': {'num': '484', 'dato': "jul-sep"},
            #    '040': {'num': '485', 'dato': "maj-jul"},
            #    '056': {'num': '486', 'dato': "aug-dec"},
            #    '028': {'num': '461', 'dato': "apr15"},
            #    '032': {'num': '462', 'dato': "apr30"},
            #    '036': {'num': '463', 'dato': "maj15"},
            #    '040': {'num': '464', 'dato': "maj31"},
            #    '044': {'num': '465', 'dato': "jun15"},
            #    '048': {'num': '466', 'dato': "jul15"},
            #    '056': {'num': '467', 'dato': "aug15"},
            #    '060': {'num': '468', 'dato': "sep15"},
            #    '064': {'num': '469', 'dato': "okt15"},
            #    '070': {'num': '470', 'dato': "nov15"},
            #    '074': {'num': '471', 'dato': "dec15"},
            #    '084': {'num': '452', 'dato': "nov10"},
            #    '088': {'num': '453', 'dato': "nov25"},
            #    '096': {'num': '454', 'dato': "dec25"},

               }
for indekso, dosieroj in indeksoj.items():
    dosieroj.sort()
    if indekso == 'Haupttext deutsch':
        pdf_bildoj = []
        for d in dosieroj:
            reg = "(.+)_page(.+){0}".format(image_extension)
            m1 = re.search(reg, d, re.I)
            if m1:
                nombro_pagxo = m1.group(2)
                pdf_bildoj.append(d)
                if nombro_pagxo in dividi_jaro:
                    num = dividi_jaro[nombro_pagxo]['num']
                    dato = dividi_jaro[nombro_pagxo]['dato']
                    pdf_nomo = "{0}_{1}_n{2}_{3}".format(albumo, jaro, num, dato)
                    krei_pdf(pdf_bildoj, pdf_nomo)
                    pdf_bildoj = []

            else:
                print('Fuŝo kun regeksprimo je:', d)
                b
    else:

        krei_pdf(pdf_bildoj=dosieroj, pdf_nomo=indekso)
