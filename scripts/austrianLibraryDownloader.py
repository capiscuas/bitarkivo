#!/usr/bin/env python
# -*- coding: utf-8 -*-

#Archive.org
from internetarchive import get_item
from os import walk
from os import path
import re
import json, codecs
import bs4
from urllib.request import urlopen

#for i in range(1,40):
    #url = 'https://babel.hathitrust.org/cgi/imgsrv/image?id=nyp.33433089904589;seq='+str(i)+';width=200'
    #print 'wget "'+url+'" -O page'+str(i).zfill(3)+".jpg"

#bnjp.32101076180585
#http://babel.hathitrust.org/cgi/imgsrv/image?id=nyp.33433089904589;seq=61;width=300
#http://babel.hathitrust.org/cgi/pt?id=nyp.33433075914329;view=1up;seq=191

#for i in range(93,113):
    #url = 'https://babel.hathitrust.org/cgi/imgsrv/image?id=njp.32101076180585;seq='+str(i)+';width=2000'
    #print 'wget "'+url+'" -O page'+str(i).zfill(3)+".jpg"


def getAvailableYears(album):
    html = urlopen('http://anno.onb.ac.at/cgi-content/anno-plus?apm=0&aid='+album).read()
    #print html
    soup = bs4.BeautifulSoup(html, "lxml")
    #print soup
    #boccat = soup.find("dl", "boccat")
    months = {}
    previous_month = ''

    div = soup.find("div","view-year")
    results = div.findAll("td", "active")
    albumyears = []
    for result in results:
        albumyears.append(result.a.text)

    print("years = [{0}]".format(",".join(albumyears)))
    return albumyears
    #label = soup.find("label",page_name)

def getTotalPages(section_url, page_name, keynumber):
    html = urlopen(section_url).read()
    #print html
    soup = bs4.BeautifulSoup(html, "lxml")
    #print soup
    #boccat = soup.find("dl", "boccat")


    #label = soup.find("label",page_name)
    #print results
    first_page = 10000
    highest_page = 0
    try:
        print(keynumber)
        select = soup.find("select", {"id": "v{0}".format(keynumber)})
    #print select

        for option in select.findChildren():
            value = int(option["value"])

            if value != -1 and value < first_page:
                first_page = value
            if value > highest_page:
                highest_page = value

        results = soup.find("label", {"title" : page_name})
        span = results.parent
        select = span.parent.form.select

        for option in select.findChildren():
            value= int(option["value"])
            if value != -1 and value < first_page: first_page = value
            if value > highest_page: highest_page = value

    except:
        div = soup.find("div","prevws")

        for a in div.findAll("a"):
            href =  a['href']
            m = re.search('.+&page=(\d+).+', href)
            if m:
                page = int(m.group(1))
                if page < first_page:   first_page = page
                if page > highest_page:   highest_page = page

    print("pag {0} -> {1}".format(first_page, highest_page))
    return first_page, highest_page


def get_category_links(year,section_url):
    lines = []
    html = urlopen(section_url).read()
    #print html
    soup = bs4.BeautifulSoup(html, "lxml")
    #print soup
    #boccat = soup.find("dl", "boccat")
    months = {}
    previous_month = ''

    for dl in soup.findAll("dl"):
        title =  dl.dt.h2["title"]
        print(title)
        href = dl.dd.a["href"]
        src =   dl.dd.img["src"]
        k = re.search('.+'+collection+'\/\d+\/\d\d\d\d(\d+)\/\d+\.png',src)#/espview/e2c/1906/19060061/00000001.png
        m = re.search('.+&pos=(\d+).+', href)
        if m and k:
            initpage = int(m.group(1))
            keynumber = k.group(1)
            month_url = 'http://anno.onb.ac.at/cgi-content/anno-plus?aid='+collection+'&datum='+str(year)+'&pos='+str(initpage)+'&size=45'
            #print month_url
            init,end = getTotalPages(month_url, title, keynumber)
            months[title] = {'init':initpage,'end':end,'key':keynumber}

            #print 'Starts page',initpage
        else:
            print(href, src)
            print('error with regex')

    #category_links = [BASE_URL + dl.a["href"] ]
    print(months)
    #b
    for month_name,values in months.items():
        init = values['init']
        end = values['end']
        key = values['key']
        total_pages = end - init + 1
        for i in range(1,total_pages+1):
            #if i > 60: continue
            link = 'http://anno.onb.ac.at/cgi-content/annoshow-plus?call='+collection+'|'+str(year)+'|'+key+'|'+str(i).zfill(8)+'||jpg||80|'
            lines.append('wget "'+link+'" -O "'+month_name+'_page'+str(i).zfill(3)+'.jpg"')
    return lines

collection = 'e0f' #Kosmoglott
#collection = 'e0g' #Kosmoglott
#collection = 'e2c' #Amerika Esperantisto
#collection =  'e0i' #Amikeco
#collection =  'e2k' #Belga Esperantisto
#collection =  'e2o' #Hispana Esperantisto

years = []
#years =["1929", "1938"]
# years = ["1912"]
#url = 'http://anno.onb.ac.at/cgi-content/anno-plus?aid=e2c'

if not years:
  years = getAvailableYears(collection)

#print get_category_links(url)
upload = codecs.open("commandsDownloadAustrian.autogenerated.txt", "w", "utf-8")

for year in years:
    url = 'http://anno.onb.ac.at/cgi-content/anno-plus?aid='+collection+'&datum='+str(year)+'&size=45'
    print(year)
    yearlines = get_category_links(year,url)
    upload.write('mkdir '+str(year)+'\n')
    upload.write('cd '+str(year)+'\n')
    for line in yearlines:
        #link = 'http://anno.onb.ac.at/cgi-content/annoshow-plus?call=e0i|'+str(year)+'|0005|'+str(i).zfill(8)+'||jpg||100|'
        upload.write(line)
        print(line)
        upload.write('\n')
    upload.write('cd ..\n')
    upload.write('\n')
    upload.write('\n')
upload.close()
