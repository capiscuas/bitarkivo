#!/usr/bin/env python
# for i in *.pdf; do python xxxxxx.py <$i >fixed/$i
import copy, sys
from pyPdf import PdfFileWriter, PdfFileReader

inputfile =  sys.argv[1]
outputfile =  sys.argv[2]
input = PdfFileReader(open(inputfile))
if input.isEncrypted:
    input.decrypt('')

output = PdfFileWriter()
outlines = input.getOutlines()
output.canv.addOutlineEntry(outlines)
output.write(outputfile)
