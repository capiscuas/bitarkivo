#!/usr/bin/env python
# -*- coding: utf-8 -*-


import pprint
from internetarchive import get_item
import sys, traceback
import xml.etree.ElementTree as ET

import codecs
import csv,re

suragata = {'ĉ':'cx','ĝ':'gx','ĥ':'hx','ĵ':'jx','ŝ':'sx','ŭ':'ux','!':''}
monatoj = ["","jan","feb","mar","apr","maj","jun","jul","aug","sep","okt","nov","dec"]
magazines = {}


with open('data.csv', 'rb') as csvfile:
    spamreader = csv.reader(csvfile, delimiter='|', quotechar='~')
    for row in spamreader:
         #print row
         
         collection_ref = row[0]
         release_date = row[2]
         try:
            lang = row[3]
         except:
             continue
         item_ref = row[4]
         collection_name = row[5]
         original_collection_name = collection_name
         item_year_number = row[6]
         urls = eval(row[7])
         release_year = release_date[:4]
         release_month = ''
         release_day = ''
         number = ''
         if '-' in release_date:
            if '-' in release_date[5:]:
                release_month = int(release_date[5:7])
                release_day = int(release_date[8:])
                #print collection_name_simple, item_year_number
                #print row
            else:
                release_month = int(release_date[5:])
         
         
         for i,ix in suragata.items():
             collection_name = re.sub(i,ix , collection_name)
         
         #collection_name = strip_accents(collection_name)
         collection_name = collection_name.title()
         collection_name_simple = re.sub('[\s+|:|\-]', '', collection_name)
         
         m = re.search('\[*(\d+)\]*,\s+n\.\s+([\d|-]+).*', item_year_number)
         if m:
            year = int(m.group(1))
            number = m.group(2)
            #if '-' in number:
                #print collection_name_simple, item_year_number
                #print urls

            #print 'Year',year, 'Number',number
         else:
             m = re.search('([\d|-]+)', item_year_number)
             year = int(m.group(1))
             #print 'Year',year, ' No number'
             
             #print urls
             #number = int(m.group(2))
         

         name_end = ''
         
         name_date = '('+str(year) + ')'
        
             
         if number:
             name_end = '_n' +number.zfill(3)
             
         if release_month:
             name_end +=  '_' +monatoj[release_month]  
         if release_day:
             name_end +=  str(release_day).zfill(2)
         
         filename = collection_name_simple + '_' + name_date + name_end
         numero = {'release_year':year,
                   'release_day':release_day,
                   'release_month':release_month,
                   'numero':number,
                   'item_ref':item_ref,
                   'filename':filename}
          
            
         #print filename, urls
         
         
         if not collection_name_simple in magazines.keys():
              oldest_date = 300000
              magazines[collection_name_simple] = {'title':original_collection_name,'language':'[eo]','firstdate':'','issn':'','oclc':'','website':'','country':'','editor':'','editor_title':'','category':'','company':'','collection_name_simple':collection_name_simple,'znumeroj':[numero]}  
              #print 'mkdir',collection_name_simple
         else:
             magazines[collection_name_simple]['znumeroj'].append(numero)
             
         if release_month: monato = release_month
         else: monato = 0
         
         nova_dato = int(year)*100+int(monato)
         if oldest_date > nova_dato:
             oldest_date = nova_dato
             magazines[collection_name_simple]['firstdate'] = str(year)+'|'+str(release_month)+'|'+str(release_day)
              
         
         for i,url in enumerate(urls):
             filepath = '"'+collection_name_simple +'/'+ filename
             if i>1:
                filepath += '_'+i
             filepath += '.pdf"'
             #print 'wget', url,'-O',filepath


for shorttitle,mag in magazines.items():
    print shorttitle
    numbers = []
    peryear = False
    hasnumbers = True
    for n in mag['znumeroj']:
        if n['numero']:
            num = n['numero']
            hasnumbers = True

            if num in numbers:
                peryear = True
                break
            else:
                numbers.append(num)
        else:
            hasnumbers = False
        
    
    
    if shorttitle in ['Boletin','FidoKajEspero','HispanaEsperantisto','FlandraEsperantisto','EuxropaBulteno','LaRondo']:
        peryear = False
        
    years = []
    not_found_years = []
    for n in mag['znumeroj']:
        year = int(n['release_year'])
        years.append(year)
        # numbers[]
    unique_years = list(set(years))
    print unique_years
    first_year = unique_years[0]
    last_year = unique_years[-1]
    #print range(first_year,last_year+1)
    for y in range(first_year,last_year+1):
        if y not in unique_years:
            not_found_years.append(y)
            print 'Year ',y,'not found'
            
    if hasnumbers:
        if not peryear:
            all_numbers = []
            for n in mag['znumeroj']:
                #print n['numero']
                numeros = n['numero'].split('-')
                for nu in numeros:
                    if nu:
                        all_numbers.append(int(nu))
            all_numbers.sort()
            first_number = all_numbers[0]
            last_number = all_numbers[-1]
            
            print 'All numbers',all_numbers
            for y in range(first_number,last_number+1):
                if y not in all_numbers:
                    print 'Number ',y,'not found'
        else:
            print 'Is Per Year'
            numbers_per_year = {}
            all_numbers = []
            for n in mag['znumeroj']:
                #print n['numero']
                year = int(n['release_year'])
                
                if not numbers_per_year.has_key(year):
                    numbers_per_year[year] = []
                    
                numeros = n['numero'].split('-')
                for nu in numeros:
                    if nu:
                        all_numbers.append(int(nu))
                        numbers_per_year[year].append(int(nu))
            all_numbers.sort()
            first_number = all_numbers[0]
            last_number = all_numbers[-1]
            
            #print 'All numbers',all_numbers
            for i,j in numbers_per_year.items():
                if not j:#si esta vacio, ese año esta juntado
                   print 'Year',i,'solo un PDF sin numeros'
                else:
                    for c in range(first_number,last_number+1):
                        if c not in j:
                            print 'Number ',c,'of year',i,'not found'
                
        
#import json
#with open('magazines.json', 'w') as fp:
    #json.dump(magazines, fp,sort_keys=True,indent=4, separators=(',', ': '))
