#!/usr/bin/env python
# -*- coding: utf-8 -*-

from dspace.repository import Repository
from sickle import Sickle 
from sickle.iterator import OAIResponseIterator
import pprint
from internetarchive import get_item
import sys, traceback
import xml.etree.ElementTree as ET

import codecs
#with codecs.open(filename,'r',encoding='utf8') as f:
    #text = f.read()
# process Unicode text
file = codecs.open('data.csv','w','utf-8')
   
                   
def getUrl(root):
    file_urls = []
    for child in root[1]:
    #print child.tag, child.attrib
        for grandchild in child:
            #print grandchild.tag, grandchild.attrib
            if 'fileSec' in grandchild.tag:
                for a in grandchild:
                    #print a.tag, a.attrib
                    if 'fileGrp' in a.tag:
                        if a.attrib['USE'] == 'ORIGINAL':
                            for ab in a:
                                file_url = ab.attrib['OWNERID']
                                file_urls.append(file_url)
                            
    return file_urls


#for afile in xml.findall("{http://www.loc.gov/METS/}fileSec"):
    #print afile#.keys()#OWNERID

'fileSec'

#Biboteko repository
pp = pprint.PrettyPrinter(indent=4)
sickle = Sickle('http://esperanto.es:8080/oai/request')#,iterator=OAIResponseIterator)

repo = Repository('http://esperanto.es:8080/oai/request', base_url='http://esperanto.es:8080')

tree = ET.parse('test.xml')
xml = tree.getroot()

#set_id = "hdl_11013_4660" 
#records = sickle.ListRecords(metadataPrefix='mets', set=set_id) #all the elements of a set
#while True:
            #record = records.next()
            #print 'aaa'
            #if hasattr(record, 'metadata'):
                #titleinfo = record.metadata['titleInfo'][0]

            
#b    

#Use http://validator.oaipmh.com/ to check online the server status and getCollections
#OAI XML URL: http://esperanto.es:8080/oai/request

#print list(repo.getItemHandles())

#items = repo.getItem(identifier='11013/4771')
#print items

#print repo.getItem(handle='11013/4681')
#print list(repo.getItems(collection='11013/3472'))

#records = sickle.ListRecords(metadataPrefix='hdl')
#records.next()
#r = sickle.GetRecord(identifier='oai:localhost:11013/1374',metadataPrefix='mets')
#print r

#Archive.org
#ia_item = get_item('hdl_11013_90')
#md = dict(mediatype='text', creator='AAAAn')
#filename = '/tmp/SABE_UD_ESPERANTO.pdf'
#print 'Uploading ',filename
#ia_item.upload(filename, metadata=md)

magazines = {}
sets = sickle.ListSets()


#http://esperanto.es:8080/jspui/handle/11013/4771


import xml.etree.ElementTree as ET 
while True:
        try:
                myset = sets.next()
        except:
                break
        #print dir(myset)
        #print myset.params
        #Si contine puntito es de la Biblioteko, el resto son Gazeto
        set_id = myset.setSpec
        
        catName =  myset.setName
        if not u'·' in catName: #is not a magazine
        #print myset.raw
            
            total_items = 0
        #Obtener descripcion algunas sets como http://esperanto.es:8080/jspui/handle/11013/1374
            #set_id = "hdl_11013_4660" 
            records = sickle.ListRecords(metadataPrefix='mets', set=set_id, ignore_deleted=True) #all the elements of a set
            while True:
                    try:
                        record = records.next()
                    except:
                        break
                    #if record.metadata['genre'][0] == 'Revuo':
                    
                    #
                    #root = ET.fromstring(xml)
                    

                        #traceback.print_exc()
                        
                    #for item in xml.findall("{http://www.loc.gov/METS/}fileSec/{http://www.loc.gov/METS/}fileGrp"):
                        #print item#.keys()#OWNERID
                    #print record.metadata['identifier'][0]
                    #
                    #print dir(record)
                    #print record.metadata
                    #try:
                    urls = []
                    dateIssued = ''
                    languageTerm = ''
                    identifier = ''
                    if hasattr(record, 'metadata'):
                        #print record.metadata.items()
                        titleinfo = record.metadata['titleInfo'][0]
                        if record.metadata.has_key('dateIssued'):
                            dateIssued = record.metadata['dateIssued'][0]
                        if record.metadata.has_key('languageTerm'):
                            languageTerm = str(record.metadata['languageTerm'])
                        if record.metadata.has_key('identifier'):
                            identifier = record.metadata['identifier'][0]
                        xml =  record.xml
                        urls = getUrl(xml)
                    else:
                        print 'NO XML'
                        continue
                
                        #ET.dump(xml)
                    #except:
                    #print 'No metadata',record
                    #continue
                    
                    
                    #if not len(urls):
                        #print 'Not urls found'
                        #Aaaa
                    total_items += 1
                    tmagazine = titleinfo[:titleinfo.find(',')]
                    tnumber = titleinfo[titleinfo.find(',')+2:]
                    
                    
                    line = set_id + u'|'  + catName  + u"|"  + dateIssued + u"|"  + languageTerm + u"|"  + identifier + u"|" + tmagazine + u"|" + tnumber + u"|" + str(urls)
                    print line
                    file.write(line)
                    file.write(u"\n")
                    #if magazines.has_key(tmagazine):
                        #magazines[tmagazine].append(tnumber)
                    #else:
                        ##create folder
                        #magazines[tmagazine] = []
                            
                        #download file in that folder
            if not total_items:
                line = set_id+u'|'+catName+u'|EMPTY'
                print line
                file.write(line)
                file.write(u"\n")
    #except:
        #traceback.print_exc()

file.close()
#print "Downloaded books"
#for numbers, mag in magazines.items():
   #print mag
   #print numbers

##myset.raw.encode('utf8')
#colection_id = set.header.identifier
#u'<set xmlns="http://www.openarchives.org/OAI/2.0/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><setSpec>hdl_11013_4638</setSpec><setName>Adresaro [1957?-1962?]</setName></set>'
#
##r = sickle.GetRecord(identifier='oai:localhost:11013/4639',metadataPrefix='mets')
#
#record.raw.encode('utf8') #Convertir a estructura a partir del XML e 
##identificar Coleccion y Metadata:URL PDF, URL imagen, dc.language.iso, dc.date.accessioned, dc.date.available, dc.date.issued, dc.identifier.uri
##dc.title , dc.type, dc.subject, europeana.type, europeana.rights, languageTerm
#item = repo.getItem(handle='11013/2398')
##hay algunos items que contienen varios ficheros:
##http://esperanto.es:8080/jspui/handle/11013/4771

##item = repo.getItem(identifier='11013/2398')
##items = repo.getItems(collection=handle)
#print item
#for i in items:
  #pp.pprint(i)
  #print i