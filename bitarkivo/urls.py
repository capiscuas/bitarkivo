# -*- coding: utf-8 -*-

from django.contrib import admin
from django.http import HttpResponseRedirect

from django.conf.urls import include, url

admin.autodiscover()


from django.views.generic import TemplateView
from bitarkivo.apps.gazetoj.views import GazetListVido
from bitarkivo.apps.kontakto.views import KontaktVido

urlpatterns = [
    # url(
    #     r'^$',
    #     TemplateView.as_view(template_name="hejmpagxo.html"),
    #     name='hejmpaĝo'
    # ),
    url(r'^$', lambda x: HttpResponseRedirect('/gazetoj/'), name='hejmo'),
    url(r'^gazetoj/', include('bitarkivo.apps.gazetoj.urls')),
    url(r'^kontaktu/$', KontaktVido, name='kontakto'),
    url(r'^pri/$', TemplateView.as_view(template_name='pri.html'), name='pri'),
    url(r'^admin/', admin.site.urls),
]
