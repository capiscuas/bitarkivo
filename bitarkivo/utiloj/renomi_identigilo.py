import argparse
import os
import os.path
import sys

from internetarchive import (
    get_item,
    download,
    upload,
    modify_metadata
)

parser = argparse.ArgumentParser()
parser.add_argument("malnova_id")
parser.add_argument("nova_id")
args = parser.parse_args()
NUR_METADATOJ = True
item = get_item(args.malnova_id)

if not item:
    print(args.malnova_id, 'ne ekzistas.')
else:

    metadatoj = item.item_metadata['metadata']
    gardonta_metadatoj = {'creator': metadatoj['creator'],
                          'subject': metadatoj['subject'],
                          'year': metadatoj['year'],
                          'date': metadatoj['date'],
                          'notes': metadatoj['notes'],
                          'language': metadatoj['language'],
                          'description': metadatoj['description'],
                          'identifier': args.nova_id}
    if not NUR_METADATOJ:
        download(args.malnova_id, verbose=True, formats=['Text PDF'])

        for filename in os.listdir(args.malnova_id):
            if filename.endswith(".pdf"):
                elsxutita_pdf = os.path.join(args.malnova_id, filename)
                nova_pdf = "{0}.pdf".format(args.nova_id)
                r = upload(args.nova_id, files={nova_pdf: elsxutita_pdf})
                if r[0].status_code == 200:
                    print('Alŝutado sukcesis.')
                else:
                    print('Fuŝo dum la alŝutado.', r[0].status_code, r[0].content)

                os.remove(elsxutita_pdf)
                os.rmdir(args.malnova_id)


    r = modify_metadata(args.nova_id, metadata=gardonta_metadatoj)
    if r.status_code == 200:
        print('Metadatoj sukcese ŝanĝitaj.')
    else:
        print('Fuŝo ŝanĝinte metadatojn.', r.status_code, r.content)
