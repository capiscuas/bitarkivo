# -*- coding: utf-8 -*-


from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic import TemplateView
from django.utils import timezone
from django.http import HttpResponse, HttpResponseRedirect
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from bitarkivo.apps.gazetoj.models import Gazeto, Numero, Lingvo


class GazetListVido(ListView):

    model = Gazeto
    template_name = "gazetoj/gazetoj_listo.html"

    def render_to_response(self, context, **response_kwargs):
        response = super(GazetListVido, self).render_to_response(context, **response_kwargs)
        parametroj = self.request.GET
        if 'list_vido' in parametroj:
            response.set_cookie('list_vido', parametroj['list_vido'])
        return response

    def get_context_data(self, **kwargs):
        context = super(GazetListVido, self).get_context_data(**kwargs)
        # context['now'] = timezone.now()
        filtroj = self.request.GET

        list_vido = self.request.COOKIES.get('list_vido')
        filtroj = self.request.GET
        if 'list_vido' in filtroj :
            list_vido = filtroj['list_vido']
        else:
            list_vido = self.request.COOKIES.get('list_vido')
        try:
            list_vido = bool(int(list_vido))
        except:
            list_vido = False
        context['list_vido'] = list_vido

        if 'lingvo' in filtroj:
            lingvo = Lingvo.objects.get(iso3=filtroj['lingvo'])
            context['filtro'] = 'Lingvo: {0}'.format(lingvo.nomo)
        elif 'q' in filtroj:
            context['filtro'] = 'Nomserĉo: {0}'.format(filtroj['q'])
        elif 'lando' in filtroj:
            context['filtro'] = 'Lando: {0}'.format(filtroj['lando'])
        else:
            context['filtro'] = 'Ĉiuj'

        total_numeroj = 0
        for g in Gazeto.objects.all():
            total_numeroj += g.numeroj.count()
        context['total_numeroj'] = total_numeroj

        return context

    def get_queryset(self):
        filtroj = self.request.GET

        if 'lingvo' in filtroj:
            gazetoj = Gazeto.objects.filter(lingvoj=filtroj['lingvo'])
        elif 'q' in filtroj:
            gazetoj = Gazeto.objects.filter(nomo__icontains=filtroj['q'])
        elif 'lando' in filtroj:
            gazetoj = Gazeto.objects.filter(landoj=filtroj['lando'])
        else:
            gazetoj = Gazeto.objects.all()

        return gazetoj


class GazetDetajlVido(DetailView):

    model = Gazeto
    paginate_by = 20

    def render_to_response(self, context, **response_kwargs):
        response = super(GazetDetajlVido, self).render_to_response(context, **response_kwargs)
        parametroj = self.request.GET
        if 'list_vido' in parametroj:
            response.set_cookie('list_vido', parametroj['list_vido'])
        return response

    def get_context_data(self, **kwargs):
        context = super(GazetDetajlVido, self).get_context_data(**kwargs)

        filtroj = self.request.GET

        if 'list_vido' in filtroj :
            list_vido = filtroj['list_vido']
        else:
            list_vido = self.request.COOKIES.get('list_vido')
        try:
            list_vido = bool(int(list_vido))
        except:
            list_vido = False
        context['list_vido'] = list_vido

        jaro = None
        jaroj = [x.year for x in context['gazeto'].numeroj.dates('dato', 'year')]

        context['nuna_jaro'] = None
        context['jaroj'] = jaroj
        context['havas_sekvantan'] = False
        context['havas_antauxan'] = False


        try:
            jaro = filtroj.get('jaro')
            jaro = int(jaro)
        except:
                pass

        if not jaro and jaroj:
            jaro = jaroj[0]

        if jaro in jaroj:
            numeroj = context['gazeto'].numeroj.filter(dato__year=jaro)
            context['nuna_jaro'] = jaro
            indekso_jaro = jaroj.index(jaro)
            if indekso_jaro > 0:
                context['antauxa_jaro'] = jaroj[indekso_jaro - 1]
                context['havas_antauxan'] = True

            if indekso_jaro < len(jaroj) -1:
                context['sekvanta_jaro'] = jaroj[indekso_jaro + 1]
                context['havas_sekvantan'] = True

        if not context['nuna_jaro']:
            numeroj = context['gazeto'].numeroj.all()
        context['numeroj'] = numeroj

        return context


class NumerDetajlVido(DetailView):
    model = Numero
    template_name = "gazetoj/numero_detajlo.html"

    def get_context_data(self, **kwargs):
        context = super(NumerDetajlVido, self).get_context_data(**kwargs)
        if 'elsxutu' in self.request.GET:
            context['elsxutu'] = True
        context['now'] = timezone.now()
        return context

    def get_object(self):
        gazeto = Gazeto.objects.get(slug=self.kwargs.get("gazeto_slug"))
        return Numero.objects.get(gazeto=gazeto, indekso=self.kwargs.get("indekso"))

#
# class NumerDetajlElsxuto(DetailView):
#
#     model = Numero
#
#     def get(self, request, gazeto_slug=None, indekso=None):
#         gazeto = Gazeto.objects.get(slug=self.kwargs.get("gazeto_slug"))
#         numero = Numero.objects.get(gazeto=gazeto, indekso=self.kwargs.get("indekso"))
#         url = 'https://ia800204.us.archive.org/10/items/AktivAmeriko_2002_n008_jun/AktivAmeriko_(2002)_n008_jun.pdf'
#         # response = HttpResponse(extension='.pdf')
#         # response['Content-Disposition'] = 'attachment; filename="manual_protocolo.pdf"' %manual_protocolo
#         return HttpResponseRedirect(url)
