import logging
import datetime
from internetarchive import get_item, search_items



from django.core.management.base import BaseCommand

from bitarkivo.apps.gazetoj.models import (
    Gazeto,
    Numero,
    Lingvo
)

from bitarkivo.apps.gazetoj.iloj import obteni_numerdetajloj, obteni_numero_sekreta, sxargxi_gazetoj

FORMAT = "%(levelname)s %(asctime)s %(funcName)s %(lineno)d %(message)s"
logging.basicConfig(level=logging.DEBUG, format=FORMAT)


class Command(BaseCommand):
    help = "Aldonas novan gazeton kiu jam ekzistas en archive.org"

    def add_arguments(self, parser):
        parser.add_argument('slug', type=str)
        parser.add_argument(
            '--listdosiero',
            default=False,
            dest='listdosiero',
            help='Ŝarĝi la liston de aĝoj el dosiero',
        )
        parser.add_argument(
            '--rekrei',
            action='store_true',
            dest='rekrei',
            default=False,
            help='Rekrei la gazeto(jn)',
        )

        parser.add_argument(
            '--bildoj',
            action='store_true',
            dest='bildoj',
            default=False,
            help='Obteni bildojn kaj PDF mezurojn',
        )

    def handle(self, *args, **options):
        slugnomo = options['slug']
        rekrei = options['rekrei']
        profunde = options['bildoj']
        listdosiero = options['listdosiero']
        logging.debug('title:{0}_'.format(slugnomo))

        gazetdosiero = "/home/ivan/workspace/bitarkivo/gazetoj.ods"
        gazetoj = sxargxi_gazetoj(gazetdosiero)

        for slug, g in gazetoj.items():
            jaro_fino = g['jaro_fino']
            jaro_komenco = g['jaro_komenco']
            if g['jaro_fino']:
                dato_fino = datetime.date(jaro_fino, 1, 1)
            else:
                dato_fino = None

            if g['jaro_komenco']:
                dato_komenco = datetime.date(jaro_komenco, 1, 1)
            else:
                dato_komenco = None

            if slug.lower() == slugnomo.lower() or slugnomo == "cxiuj" and g['reta']:
                logging.debug('Aldonante en la datumbazo', slug, g)

                if g['cxu_kompleta'].lower() == 'kompleta':
                    kompleta = True
                    mankantaj_numeroj_tkst = ""
                    mankantaj_numeroj_num = 0
                else:
                    kompleta = False
                    mankantaj_numeroj_tkst = g['cxu_kompleta']
                    mankantaj_numeroj_num = g['total_num_mankantaj']

                try:
                    nova_gazeto = Gazeto.objects.get(slug=slug)
                    if rekrei:
                        nova_gazeto.delete()
                except Gazeto.DoesNotExist:
                    nova_gazeto = None

                if not nova_gazeto or rekrei:
                    nova_gazeto = Gazeto.objects.create(slug=slug)
                    logging.debug('Kreante novan registron')

                nova_gazeto.nomo = g['nomo']
                logging.debug(nova_gazeto.nomo)
                nova_gazeto.priskribo = g['priskribo']
                nova_gazeto.dato_komenco = dato_komenco
                nova_gazeto.dato_fino = dato_fino
                nova_gazeto.cxu_kompleta = kompleta
                nova_gazeto.mankantaj_numeroj_tkst = mankantaj_numeroj_tkst
                nova_gazeto.mankantaj_numeroj_num = mankantaj_numeroj_num
                nova_gazeto.save()

                if g['lingvoj']:
                    lingvoj = g['lingvoj'].split(',')
                    for l in lingvoj:
                        ling = Lingvo.objects.get(iso3=l)
                        nova_gazeto.lingvoj.add(ling)

                if listdosiero:
                    items = []
                    with open(listdosiero) as f:
                        lines = f.read().splitlines()
                        for l in lines:
                            logging.debug(l)
                            items.append(l)
                else:
                    logging.debug('identifier:{0} uploader:hoshiko@riseup.net'.format(slug))
                    if profunde:
                        items = search_items('{0}_ uploader:hoshiko@riseup.net'.format(slug)).iter_as_items()
                    else:
                        items = search_items('{0}_ uploader:hoshiko@riseup.net'.format(slug))

                jpg = ""
                for c, item in enumerate(items):
                    dosiergrando = 0
                    dosiernomo = ""
                    logging.debug(item)
                    logging.debug(c)


                    if profunde:
                        if listdosiero:
                            item = get_item(item)
                        for f in item.files:
                            if f['format'] == 'Text PDF':
                                    dosiergrando = f['size'] #Bytoj
                                    dosiernomo = f['name']
                                    dosierosenpdf = dosiernomo[:-len(".pdf")]

                        identigilo = item.identifier
                    else:
                        if listdosiero:
                            identigilo = item
                        else:
                            identigilo = item['identifier']

                    if identigilo.startswith('{0}_'.format(slug)):
                        if profunde:
                            metadata = item.item_metadata
                            if 'd1' in metadata:
                                serviladreso = metadata['d1']
                            elif 'server' in metadata:
                                serviladreso = metadata['server']
                            elif 'd2' in metadata:
                                serviladreso = metadata['d2']
                            sekreta_num = obteni_numero_sekreta(identigilo)
                            jpg = "https://{0}/BookReader/BookReaderImages.php?zip=/{1}/items/{2}/{3}_jp2.zip&file={3}_jp2/{3}_0000.jp2&scale=4&rotate=0".format(
                                  serviladreso, sekreta_num, identigilo, dosierosenpdf)
                            logging.debug(dosiernomo)
                            logging.debug(jpg)
                        if c == 0:
                            nova_gazeto.url_bildo = jpg
                            nova_gazeto.save()
                        detajloj = obteni_numerdetajloj(identigilo)
                        logging.debug(detajloj)
                        if not detajloj['monatoj']:
                            dato = None
                        else:
                            dato = detajloj['unua_monato']
                            #dato = datetime.date(detajloj['jaro'], detajloj['unua_monato'], 1)

                        indekso = identigilo[len('{0}_'.format(slug)):]
                        url_pdf_arkivo = "https://archive.org/download/{0}/{1}".format(identigilo, dosiernomo)
                        url_bildo = jpg

                        n, created = Numero.objects.get_or_create(gazeto=nova_gazeto,
                                                                  indekso=indekso)

                        n.dato = dato
                        n.id_arkivo = identigilo
                        n.priskribo = detajloj['priskribo']
                        n.url_pdf_arkivo = url_pdf_arkivo
                        n.url_bildo = url_bildo
                        n.dosiergrando = dosiergrando
                        n.save()
