import os
from os import path
import codecs
from PyPDF2 import PdfFileMerger, PdfFileReader

from django.core.management.base import BaseCommand

from bitarkivo.apps.gazetoj.iloj import (
    sxargxi_gazetoj,
    _sxerpi_metadatoj
)


def add_metadata(fullpathpdf, updated_pdf, data):
    merger = PdfFileMerger()
    merger.append(PdfFileReader(fullpathpdf, 'rb'))
    merger.addMetadata(data)
    merger.write(updated_pdf)
    merger.close()


class Command(BaseCommand):
    help = "Aldonas novan gazeton kiu jam ekzistas en archive.org"

    def add_arguments(self, parser):
        parser.add_argument('dosierujo', type=str)
        parser.add_argument(
            '--ecigi',
            action='store_true',
            dest='ecigi',
            default=False,
            help='Ecigi la PDF-o(j)n',
        )

        parser.add_argument(
            '--arkivi',
            action='store_true',
            dest='arkivi',
            default=False,
            help='Krei la alŝutligilon por archive.org',
        )

    def handle(self, *args, **options):
        ecigi = options['ecigi']
        arkivi = options['arkivi']
        dosierujo = options['dosierujo']

        gazetdosiero = "/home/ivan/workspace/bitarkivo/gazetoj.ods"
        gazetoj = sxargxi_gazetoj(gazetdosiero)

        if arkivi:
            dosierilo_por_alsxuti = os.path.join(dosierujo, "commandsUpload.txt")
            upload = codecs.open(dosierilo_por_alsxuti, "w", "utf-8")

        files = [f for f in os.listdir(dosierujo) if f.endswith('.pdf')]
        files.sort()

        for pdf_dosiero in files:
            print(pdf_dosiero)
            metadata = {}
            pdf_metadatoj = _sxerpi_metadatoj(pdf_dosiero, gazetoj)
            gazeto_slug = pdf_metadatoj['gazeto_slug']
            gazeto = gazetoj[gazeto_slug]
            publisher = gazeto['eldonisto']
            keywords = gazeto['kradvortoj']
            language = gazeto['lingvoj']
            gazeto_titolo = gazeto['nomo']
            if not publisher or not keywords or not language or not gazeto_titolo:
                print('Missing publisher, keywords, or language or gazeto_titolo')
                import sys
                sys.exit(0)
            metadata[u'/Author'] = publisher
            metadata[u'/Creator'] = 'BitArkivo.org'
            metadata[u'/Keywords'] = keywords
            metadata[u'/Subject'] = '[Bitarkivo.org] ' + pdf_metadatoj['priskribo'] + '.'
            print(metadata[u'/Subject'])
            metadata[u'/Title'] = '[Bitarkivo.org] ' + pdf_metadatoj['titolo']
            metadata[u'dc.type'] = 'Revuo'
            metadata[u'dc.language.iso'] = language
            metadata[u'/Lang'] = language
            metadata[u'dc.date.issued'] = pdf_metadatoj['md_date']

            fullpathpdf = os.path.join(dosierujo, pdf_dosiero)
            # print(metadata)
            if ecigi:
                pdf_dosierujo = os.path.dirname(fullpathpdf)
                metadata_dir = os.path.join(pdf_dosierujo, 'metadata')
                if not os.path.isdir(metadata_dir):
                    os.mkdir(metadata_dir)
                updated_pdf = path.join(dosierujo, 'metadata', pdf_dosiero)
                add_metadata(fullpathpdf, updated_pdf, metadata)

            archive_metadata = dict(mediatype='text', creator=publisher, date=pdf_metadatoj['md_date'])
            archive_metadata[u'title'] = metadata[u'/Title']
            archive_metadata[u'dc.language.iso'] = metadata[u'dc.language.iso']
            archive_metadata[u'dc.type'] = 'Revuo'
            archive_metadata[u'description'] = metadata[u'/Subject']
            archive_metadata[u'date'] = pdf_metadatoj['md_date']
            archive_metadata[u'year'] = str(pdf_metadatoj['jaro'])
            archive_metadata[u'publicdate'] = pdf_metadatoj['dato_eldono']
            archive_metadata[u'notes'] = 'Helpu kaj kunlaboru kun <a href="http://bitarkivo.org/">BitArkivo</a> por konstrui la plej grandan arkivon de Esperantujo.'
            archive_metadata[u'ocr'] = 'ABBYY FineReader 11.0'
            archive_metadata[u'mediatype'] = 'texts'

            if arkivi:
                dosiero_sen_pdf = "".join(os.path.basename(fullpathpdf).split('.')[:-1])
                item_id = dosiero_sen_pdf
                if ecigi:
                    upload.write('ia upload ' + item_id + ' "' + updated_pdf + '"')
                else:
                    upload.write('ia upload ' + item_id + ' "' + fullpathpdf + '"')
                for k, v in archive_metadata.items():
                    upload.write(" --metadata=" + k + ":'" + v + "'")
                for lang in language.split(','):
                    upload.write(' --metadata=language:"' + lang.strip() + '"')
                for key in keywords.split(','):
                    upload.write(' --metadata=subject:"' + key.strip() + '"')
                upload.write('\n')
                upload.write('\n')
