from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand

User = get_user_model()


class Command(BaseCommand):
    def handle(self, *args, **options):
        u = User.objects.filter(email="admin@bitarkivo.org")

        if not u.exists():
            User.objects.create_superuser(
                "admin",
                "admin@bitarkivo.org",
                "cmndsntg4",
            )

            u.update(is_active=True)
