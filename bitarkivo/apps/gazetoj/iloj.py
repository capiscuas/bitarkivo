from lxml import html
import requests
import re
import logging
import datetime
from pyexcel_ods import get_data
from slugify import slugify
from django.utils.translation import activate
from django.template.defaultfilters import date

FORMAT = "%(levelname)s %(asctime)s %(funcName)s %(lineno)d %(message)s"
logging.basicConfig(level=logging.DEBUG, format=FORMAT)


def sxargxi_gazetoj(gazetdosiero):
    gazetdosiero = "/home/ivan/workspace/bitarkivo/gazetoj.ods"

    datoj = get_data(gazetdosiero)
    gazetoj_ods = datoj['Gazetoj']
    gazetoj = {}

    # en la unua vico havas la kolon-nomojn
    total_kolonoj = 11
    kolon_nomoj = gazetoj_ods[0][0:total_kolonoj]
    for g in gazetoj_ods[1:]:
        gazeto = {}
        for i, nomo in enumerate(kolon_nomoj):
            gazeto[nomo] = g[i]

        slug = gazeto["gslug"]
        if not slug:
            slug = slugify(gazeto["nomo"])
        slug = slug.strip()

        if slug not in gazetoj:
            gazetoj[slug] = gazeto
        else:
            print('Ripetita', slug, 'en la .ods dosiero.')

    return gazetoj


def _sxerpi_metadatoj(filename, gazetoj):
    m = re.search('([^_]+)_(\d+)_n([^_]+)_(.+).pdf', filename)
    #m = re.search('([^_]+)_\((\d+)\)_v(\d+)n([^_]+)_(.+).pdf', filename)
    volumen = ''
    matched = False
    numbers = []
    if m:
        release_year = int(m.group(2))
        collection = m.group(1)
        #volumen = int(m.group(3))
        numbers = m.group(3).split('-')
        months = m.group(4).split('-')
        matched = True
    else:
        m = re.search('([^_]+)_(\d+)_(.+).pdf', filename)
        if m:
            release_year = int(m.group(2))
            collection = m.group(1)
            #volumen = int(m.group(3))
            months = m.group(3).split('-')
            matched = True
        else:
            matched = False

    if matched:
        text_end = ""
        release_date, teksto_dato = sxerpi_dato(months, release_year)

        if not release_date:
            teksto_dato = ",".join(months).title()

        gazeto = gazetoj[collection]
        collection_name = gazeto['nomo']
        subject_end = ''
        md_date = ''
        release_month = None
        release_day = None
        if release_date:
            release_month = release_date.month
            release_day = release_date.day

        if release_year:
            publicdate = str(release_year)
            subject_end = u'Jaro ' + str(release_year)
            md_date = str(release_year)

        if release_month:
            md_date += '-' + str(release_month).zfill(2)
            publicdate += '-' + str(release_month).zfill(2)
            subject_end += u'. ' + teksto_dato
        else:
            publicdate += '-00'
            if teksto_dato:
                subject_end += '. ' + teksto_dato

        if release_day:
            publicdate += str(release_day).zfill(2)
            md_date += '-' + str(release_day).zfill(2)
        else:
            publicdate += '-00'
        publicdate += ' 00:00:00'
        title = collection_name + ' '
        numbers_text = ''
        if volumen:
            numbers_text = u'Volumo ' + str(volumen).zfill(2) + ', '
            title += 'v' + str(volumen).zfill(2)
        if numbers:
            if len(numbers) > 1:
                numbers_text += u'Numeroj '
            else:
                numbers_text += u'Numero '
            subject = numbers_text + '-'.join(numbers) + u' de la gazeto "' + collection_name + u'". ' + subject_end
            title += 'n' + '-'.join(numbers)
        else:
            subject = subject_end

        if months:
            title += ' (' + '-'.join(months) + ' ' + str(release_year) + ')'
    else:
        print('Not maching regexp', filename)
        cc

    metadatoj = {'gazeto_slug': collection, 'md_date': md_date,
                 'titolo': title, 'priskribo': subject,
                 'jaro': release_year, 'dato_eldono': publicdate}
    return metadatoj


def obteni_eo_monatnomoj():
    monatnomoj = []
    malgrandamonatnomoj = []
    activate('eo')

    for monato in range(1, 13):
        dato = datetime.date(2010, monato, 1)
        monatnomoj.append(date(dato, 'F'))
    return monatnomoj, malgrandamonatnomoj


malgrandamonatnomoj_ascii = ['jan', 'feb', 'mar', 'apr', 'maj', 'jun', 'jul',
                             'aug', 'sep', 'okt', 'nov', 'dec']

monatnomoj, malgrandamonatnomoj = obteni_eo_monatnomoj()


def obteni_numero_sekreta(identigilo):
    url = "https://archive.org/download/{0}".format(identigilo)
    page = requests.get(url)
    # logging.debug(page)
    tree = html.fromstring(page.content)
    indekso_teksto = tree.xpath('//h1/text()')
    return indekso_teksto[0].split("/")[1]


def sxerpi_dato(months, jaro):
    datoj = []
    if not months:
        return None, ""
    for dato in months:
        m = re.search('([^\d]+)(\d*)', dato)
        if not m:
            logging.debug(dato, 'is not a date1')
            return None, ""
        else:
            monato = m.group(1)
            if monato not in malgrandamonatnomoj_ascii:
                logging.debug(dato, 'is not a date2')
                return None, ""
            else:
                tago = m.group(2)
                monato_num = malgrandamonatnomoj_ascii.index(monato) + 1
                if tago:
                    dato = datetime.date(jaro, monato_num, int(tago))
                    dato_teksto = '{0}a de {1}'.format(int(tago), monatnomoj[monato_num - 1].title())
                else:
                    dato = datetime.date(jaro, monato_num, 1)
                    dato_teksto = '{0}'.format(monatnomoj[monato_num - 1].title())
                datoj.append({'dato': dato, 'teksto': dato_teksto})

    if len(datoj):
        if len(datoj) > 1:
            teksto = '{0} - {1}'.format(datoj[0]['teksto'], datoj[-1]['teksto'])
        else:
            teksto = datoj[0]['teksto']
    else:
        teksto = ""

    dato = datoj[0]['dato']
    return dato, teksto

def obteni_numerdetajloj(numero_nomo):
        logging.debug(numero_nomo)
        m = re.search('([^_]+)_(\d+)_n([^_]+)_(.+)', numero_nomo)
        #m = re.search('([^_]+)_\((\d+)\)_v(\d+)n([^_]+)_(.+).pdf', filename)
        # volumen = ''
        matched = False
        if m:
            release_year = int(m.group(2))
            collection = m.group(1)
            #volumen = int(m.group(3))
            numbers = m.group(3).split('-')
            months = m.group(4).split('-')
            matched = True
        else:
            m = re.search('([^_]+)_(\d+)_(.+)', numero_nomo)
            if m:
                release_year = int(m.group(2))
                collection = m.group(1)
                #volumen = int(m.group(3))
                numbers = m.group(3).split('-')
                months = []
                release_month = None
                matched = True
            else:
                logging.debug('Not maching regexp', numero_nomo)
                matched = False

        detajloj = {}

        if matched:
            teksto = ""
            release_month, teksto_monatoj = sxerpi_dato(months, release_year)
            if not release_month:  # ne estas monatoj
                aldonajxo = ",".join(months)
                months = []
                teksto = "{0}, {1}".format(
                    aldonajxo,
                    release_year)
            else:
                teksto = "Numero {0}, {1}, {2}".format(
                    "-".join(numbers),
                    teksto_monatoj,
                    release_year)

            detajloj = {'monatoj': months,
                        'numeroj': numbers,
                        'jaro': release_year,
                        'unua_monato': release_month,
                        'collection': collection,
                        'priskribo': teksto}
            return detajloj
        else:
            logging.debug('No match')
