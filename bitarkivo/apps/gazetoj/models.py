# -*- coding: utf-8 -*-

from django.db import models


# KradVorto = Hashtag
class KradVorto(models.Model):
    name = models.CharField(max_length=20)


class Lingvo(models.Model):
    nomo = models.CharField(blank=True, max_length=20, default="")
    # ISO 639-3
    iso2 = models.CharField(unique=True, max_length=3, default="")
    iso3 = models.CharField(unique=True, max_length=3, primary_key=True)

    def __str__(self):
        return "{0}:{1}".format(self.iso3, self.nomo)

    class Meta:
        verbose_name_plural = 'Lingvoj'


class Lando(models.Model):
    nomo = models.CharField(blank=True, max_length=20)
    # ISO 3166-2
    iso3 = models.CharField(unique=True, max_length=3)

    def __str__(self):
        return self.nomo

    class Meta:
        verbose_name_plural = 'Landoj'


class Persono(models.Model):
    antaux_nomo = models.CharField(blank=True, max_length=128)
    familia_nomo = models.CharField(blank=True, max_length=128)
    slug = models.SlugField(unique=True)
    url_vikipedio = models.URLField(blank=True, null=True)
    url_vikidatumo = models.URLField(blank=True, null=True)
    # Ria blogo, oficiala retpagxo, ktp
    url_oficiala = models.URLField(blank=True, null=True)


    def kompleta_nomo(self):
        return "{0}, {1}".format(self.familia_nomo, self.antaux_nomo)

    def __str__(self):
        return self.kompleta_nomo()

    class Meta:
        verbose_name_plural = 'Personoj'


class Eldonisto(models.Model):
    persono = models.ForeignKey(Persono)
    ekde_kiam = models.DateField(blank=True)
    gxis_kiam = models.DateField(blank=True)

    class Meta:
        verbose_name_plural = 'Eldonistoj'

    def __str__(self):
        return self.persono.kompleta_nomo()


class Gazeto(models.Model):
    # ekz. "Revuo Esperanto"
    nomo = models.CharField(blank=True, max_length=128)
    # memkreita per la formulo, sed modifebla, ĉar dependas de tiu uzata en archive.org
    slug = models.SlugField(unique=True)
    organizajxo = models.CharField(blank=True, max_length=128)

    # la jaroj povas esti neplenigita se ankoraŭ vivas

    # Form validators (jaro_komenco <= 1880)
    dato_komenco = models.DateField(blank=True, null=True)
    # Form validators (jaro_fino >= jaro_komenco, jaro_komenco <= jaro_now)
    dato_fino = models.DateField(blank=True, null=True)

    priskribo = models.TextField(blank=True)
    url_vikipedio = models.URLField(blank=True, null=True)
    url_vikidatumo = models.URLField(blank=True, null=True)
    url_oficiala_retpagxo = models.URLField(blank=True, null=True)
    url_bildo = models.URLField(max_length=500, blank=True, null=True)

    cxu_kompleta = models.BooleanField(blank=True, default=False)
    mankantaj_numeroj_num = models.PositiveIntegerField(blank=True, default=0)
    # En la estonteco mankantaj_numeroj estos M:M de Numeroj
    mankantaj_numeroj_tkst = models.TextField(blank=True)
    kradvortoj = models.ManyToManyField(KradVorto, blank=True)
    lingvoj = models.ManyToManyField(Lingvo, related_name="gazetoj")
    lando = models.ForeignKey(Lando, related_name="gazetoj", blank=True, null=True)
    # de vikidatumo(estontece)
    urbo = models.CharField(blank=True, max_length=128)
    eldonistoj = models.ManyToManyField(Eldonisto, blank=True)
    # num_abonantoj = IntegerField() #por statistiko kaj pli-populara ordigo
    # num_artikoloj = IntegerField() #por statistiko kaj pli-granda ordigo
    issn = models.CharField(blank=True, max_length=128)
    # num_vizitoj: IntegerField

    def display_numeroj(self):
        return ', '.join([numero.indekso for numero in self.numeroj.all()])
    display_numeroj.short_description = "Numeroj"

    def kompleta_procento(self):
        nuna_numeroj = self.numeroj.count()
        total_numeroj = nuna_numeroj + self.mankantaj_numeroj_num
        return int(nuna_numeroj * 100 / total_numeroj)

    # @property
    def unua_numero(self):
        unua_numero = self.numeroj.order_by("dato").first()
        if unua_numero.dato and not self.dato_komenco:
            dato = unua_numero.dato
        elif not unua_numero.dato and self.dato_komenco:
            dato = self.dato_komenco
        elif unua_numero.dato < self.dato_komenco:
            dato = unua_numero.dato
        else:
            dato = self.dato_komenco

        return dato

    def lasta_numero(self):
        lasta_numero = self.numeroj.order_by("dato").last()
        if lasta_numero.dato and not self.dato_fino:
            dato = lasta_numero.dato
        elif not lasta_numero.dato and self.dato_fino:
            dato = self.dato_fino
        elif lasta_numero.dato < self.dato_fino:
            dato = lasta_numero.dato
        else:
            dato = self.dato_fino

        return dato

    def __unicode__(self):
        return self.nomo


    class Meta:
        verbose_name_plural = 'Gazetoj'


class Numero(models.Model):
    # ek. 11a , 33komplemento, ktp
    indekso = models.SlugField(primary_key=True)

    gazeto = models.ForeignKey(Gazeto, related_name="numeroj")

    # Form validators (dato <= nun)
    dato = models.DateField(blank=True, null=True)
    id_arkivo = models.CharField(blank=True, max_length=200, null=True)
    url_bildo = models.URLField(max_length=500, blank=True)
    url_vikidatumo = models.URLField(blank=True)
    url_oficiala_retpagxo = models.URLField(blank=True)
    url_pdf_arkivo = models.URLField(max_length=500, blank=True)
    # URL de la txt enhavo post la OCR-procezado
    url_OCR_txt = models.URLField(max_length=500, blank=True)
    # Se estas komplementnumero kaj ne normala numero
    priskribo = models.TextField(blank=True)
    cxu_kompletento = models.BooleanField(blank=True, default=False)
    # komentoj: URL al widget/plugin komentoj
    # num_visitoj: IntegerField
    # Kiam estis alŝutita al archive.org
    # Form validators (kiam_alsxutita <= nun)
    kiam_alsxutita = models.DateTimeField(blank=True, null=True)
    # memkreita de la PDF
    num_pagxoj = models.PositiveIntegerField(blank=True, default=0)
    dosiergrando = models.PositiveIntegerField(blank=True, default=0)
    totalo_vidoj = models.PositiveIntegerField(blank=True, default=0)
    totalo_komentoj = models.PositiveIntegerField(blank=True, default=0)
    totalo_favoritoj = models.PositiveIntegerField(blank=True, default=0)

    def __str__(self):
        return "{0} / Gazeto: {1}".format(self.indekso, self.gazeto.slug)

    class Meta:
        verbose_name_plural = 'Numeroj'



class Artikolo(models.Model):
    numero = models.ForeignKey(Numero, related_name="artikoloj")
    titolo = models.CharField(blank=True, max_length=128)
    slug = models.SlugField(unique=True)
    kradvortoj = models.ManyToManyField(KradVorto, blank=True)
    # ĉu ĝi estas ĉefaj artikoloj montritaj en la kovrilo?, montri supreliste
    cxu_cxefa = models.BooleanField(blank=True, default=False)
    # La paĝnumero en la PDF kie la artikolo komencas
    pagx_numero = models.IntegerField(blank=True)
    auxtoroj = models.ManyToManyField(Persono)

    class Meta:
        verbose_name_plural = 'Artikoloj'

    def __str__(self):
        return "{0} / {1}".format(self.titolo, self.numero)
