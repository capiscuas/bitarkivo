from django.apps import AppConfig


class GazetojConfig(AppConfig):
    name = 'gazetoj'
