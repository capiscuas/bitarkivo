# -*- coding: utf-8 -*-

from django.conf.urls import url

from bitarkivo.apps.gazetoj.views import (
    GazetListVido,
    GazetDetajlVido,
    NumerDetajlVido,
)

# API
urlpatterns = [
    url(r'^$', GazetListVido.as_view(), name='vido_gazetoj'),
    url(r'^(?P<slug>[-\w]+)/$', GazetDetajlVido.as_view(), name='vido_gazeto_detaloj'),
    url(r'^(?P<gazeto_slug>[-\w]+)/(?P<indekso>[-\w]+)/$', NumerDetajlVido.as_view(), name='vido_numero_detaloj'),
    # url(r'^numero/(?P<gazeto_slug>[-\w]+)/(?P<indekso>[-\w]+)/elsxutu/$', NumerDetajlElsxuto.as_view(), name='elsxuto_numero'),
]
