# -*- coding: utf-8 -*-
# Generated by Django 1.9.11 on 2017-01-05 21:36
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('gazetoj', '0017_auto_20170105_1349'),
    ]

    operations = [
        migrations.AddField(
            model_name='numero',
            name='dosiergrando',
            field=models.PositiveIntegerField(blank=True, default=0),
        ),
    ]
