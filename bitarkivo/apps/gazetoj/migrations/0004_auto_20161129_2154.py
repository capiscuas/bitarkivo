# -*- coding: utf-8 -*-
# Generated by Django 1.9.11 on 2016-11-29 21:54
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('gazetoj', '0003_auto_20161129_2144'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='lingvo',
            name='id',
        ),
        migrations.AddField(
            model_name='lingvo',
            name='iso2',
            field=models.CharField(default='', max_length=2, primary_key=True, serialize=False, unique=True),
        ),
    ]
