# -*- coding: utf-8 -*-
# Generated by Django 1.9.11 on 2016-11-15 16:35
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Artikolo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('titolo', models.CharField(blank=True, max_length=128)),
                ('slug', models.SlugField(unique=True)),
                ('cxu_cxefa', models.BooleanField(default=False)),
                ('pagx_numero', models.IntegerField(blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='Eldonisto',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('ekde_kiam', models.DateField(blank=True)),
                ('gxis_kiam', models.DateField(blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='Gazeto',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nomo', models.CharField(blank=True, max_length=128)),
                ('slug', models.SlugField(unique=True)),
                ('organizajxo', models.CharField(blank=True, max_length=128)),
                ('jaro_komenco', models.IntegerField(blank=True)),
                ('jaro_fino', models.IntegerField(blank=True)),
                ('priscribo', models.TextField(blank=True)),
                ('url_vikipedio', models.URLField(blank=True)),
                ('url_vikidatumo', models.URLField(blank=True)),
                ('url_oficiala_retpagxo', models.URLField(blank=True)),
                ('url_bildo', models.URLField(blank=True)),
                ('cxuKompleta', models.BooleanField(default=False)),
                ('mankantaj_numeroj', models.TextField(blank=True)),
                ('urbo', models.CharField(blank=True, max_length=128)),
                ('issn', models.CharField(blank=True, max_length=128)),
                ('eldonistoj', models.ManyToManyField(to='gazetoj.Eldonisto')),
            ],
        ),
        migrations.CreateModel(
            name='KradVorto',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=20)),
            ],
        ),
        migrations.CreateModel(
            name='Lando',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nomo', models.CharField(blank=True, max_length=20)),
                ('iso3', models.CharField(max_length=3, unique=True)),
            ],
        ),
        migrations.CreateModel(
            name='Lingvo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nomo', models.CharField(blank=True, max_length=20)),
                ('iso3', models.CharField(max_length=3, unique=True)),
            ],
        ),
        migrations.CreateModel(
            name='Numero',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('indekso', models.CharField(blank=True, max_length=20)),
                ('dato', models.DateField(blank=True)),
                ('url_kovrilbildo', models.URLField(blank=True)),
                ('url_vikidatumo', models.URLField(blank=True)),
                ('url_oficiala_retpagxo', models.URLField(blank=True)),
                ('url_pdf_arkivo', models.URLField(blank=True)),
                ('url_OCR_txt', models.URLField(blank=True)),
                ('cxu_kompletento', models.BooleanField(default=False)),
                ('kiam_alsxutita', models.DateTimeField(blank=True)),
                ('stato', models.CharField(choices=[('ATA', 'Trovata'), ('OTA', 'Serĉota'), ('ITA', 'Alŝutita')], default='OTA', max_length=3)),
                ('gazeto', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='numeroj', to='gazetoj.Gazeto')),
            ],
        ),
        migrations.CreateModel(
            name='Persono',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('antaux_nomo', models.CharField(blank=True, max_length=128)),
                ('familia_nomo', models.CharField(blank=True, max_length=128)),
                ('slug', models.SlugField(unique=True)),
                ('url_vikipedio', models.URLField(blank=True)),
                ('url_vikidatumo', models.URLField(blank=True)),
                ('url_oficiala', models.URLField(blank=True)),
            ],
        ),
        migrations.AddField(
            model_name='gazeto',
            name='kradvortoj',
            field=models.ManyToManyField(to='gazetoj.KradVorto'),
        ),
        migrations.AddField(
            model_name='gazeto',
            name='lando',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='gazetoj', to='gazetoj.Lando'),
        ),
        migrations.AddField(
            model_name='gazeto',
            name='lingvoj',
            field=models.ManyToManyField(related_name='gazetoj', to='gazetoj.Lingvo'),
        ),
        migrations.AddField(
            model_name='eldonisto',
            name='persono',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='gazetoj.Persono'),
        ),
        migrations.AddField(
            model_name='artikolo',
            name='auxtoroj',
            field=models.ManyToManyField(to='gazetoj.Persono'),
        ),
        migrations.AddField(
            model_name='artikolo',
            name='kradvortoj',
            field=models.ManyToManyField(to='gazetoj.KradVorto'),
        ),
        migrations.AddField(
            model_name='artikolo',
            name='numero',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='artikoloj', to='gazetoj.Numero'),
        ),
    ]
