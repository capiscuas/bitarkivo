from django.contrib import admin
from django.db.models import Count

from bitarkivo.apps.gazetoj.models import *
# Register your models here.

#
class NumerojAdmin(admin.StackedInline):
    model = Numero
    fields = ('indekso',)
    verbose_name = 'Numero'
    verbose_name_plural = 'Numeroj'
    max_num = 0

from django import forms
from django.db.models.fields.related import ManyToManyRel
from django.contrib.admin.widgets import RelatedFieldWidgetWrapper

class AuthorForm(forms.ModelForm):

    numeroj = forms.ModelMultipleChoiceField(
        Numero.objects.all(),
    # Add this line to use the double list widget
    #    widget=admin.widgets.FilteredSelectMultiple('Books', False),
        required=False,
    )

    def __init__(self, *args, **kwargs):
        super(AuthorForm, self).__init__(*args, **kwargs)
        if self.instance.pk:
          self.initial['numeroj'] = self.instance.numeroj.values_list('pk', flat=True)
          # Add thos to lines to __init__
          rel = ManyToManyRel(Numero)
          self.fields['numeroj'].widget = RelatedFieldWidgetWrapper(self.fields['numeroj'].widget, rel, admin.site)

    def save(self, *args, **kwargs):
        instance = super(AuthorForm, self).save(*args, **kwargs)
        if instance.pk:
            for book in instance.numeroj.all():
                if book not in self.cleaned_data['numeroj']:
                    # we remove books which have been unselected
                    instance.books.remove(book)
            for book in self.cleaned_datanumeroj['numeroj']:
                if book not in instance.numeroj.all():
                    # we add newly selected books
                    instance.books.add(book)
        return instance



class GazetoAdmin(admin.ModelAdmin):
    model = Gazeto

    #form = AuthorForm


    # fieldsets = (
    #   (None, {'fields': ('nomo','slug','numeroj',)}),
    # )

    inlines = [NumerojAdmin]
    #
    # def get_numeroj(self):
    #     return "\n".join([n.indekso for n in self.numeroj.all()])
    # fields = ('nomo', 'display_numeroj')
    def nombro_numeroj(self, obj):
        return obj.nombro_numeroj

    def lingvo_listo(self, obj):
        return ",".join([l.iso2 for l in obj.lingvoj.all()])

    def get_queryset(self, request):
            return Gazeto.objects.annotate(nombro_numeroj=Count('numeroj'))

    list_display = ('nomo', 'nombro_numeroj', 'lingvo_listo')
    ordering = ('nomo',)
    search_fields = ('nomo',)
    #prepopulated_fields = {"slug": ("nomo",)}
    nombro_numeroj.short_description = 'Numernombro'
    nombro_numeroj.admin_order_field = 'nombro_numeroj'

admin.site.register(Gazeto, GazetoAdmin)
admin.site.register(Artikolo)
admin.site.register(Numero)
admin.site.register(Lingvo)
admin.site.register(Lando)
admin.site.register(Persono)
