# -*- coding: utf-8 -*-

from django.shortcuts import render
from django.template.loader import render_to_string
from django.contrib import messages
from django.core.mail import EmailMessage, send_mail
from django.shortcuts import redirect
from django.template import Context
from django.template.loader import get_template
from django.conf import settings
from django.http import HttpResponse
from bitarkivo.apps.kontakto.forms import KontaktFormularo


# add to your views
def KontaktVido(request):
    form_class = KontaktFormularo

    if request.method == 'POST':
        form = form_class(data=request.POST)

        if form.is_valid():
            contact_name = request.POST.get(
                'nomo', '')
            contact_email = request.POST.get(
                'retadreso', '')
            form_content = request.POST.get('enhavo', '')

            # Email the profile with the
            # contact information
            # template = get_template('kontakto_sxablono.txt')
            context = {
                'contact_name': contact_name,
                'contact_email': contact_email,
                'form_content': form_content,
            }
            content = render_to_string('kontakto_sxablono.txt', context)

            admin_retadreso = 'capiscuas@gmail.com'
            email = EmailMessage(
                "Nova kontakmesagxo el BitArkivo",
                content,
                "BitArkivo" + '',
                [admin_retadreso],
                headers={'Reply-To': contact_email}
            )

            email.send()
            messages.success(request, 'Via mesaĝo estis sukcese sendita.')
            return redirect('hejmo')

    return render(request, 'kontakto.html', {
        'form': form_class,
    })
