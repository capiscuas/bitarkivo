from django import forms


class KontaktFormularo(forms.Form):
    nomo = forms.CharField(required=True)
    retadreso = forms.EmailField(required=True)
    enhavo = forms.CharField(
        required=True,
        widget=forms.Textarea
    )
